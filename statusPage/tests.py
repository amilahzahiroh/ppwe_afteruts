from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import *
from .forms import FormStatus
from .models import WriteStatus
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains

class statusPageTest(TestCase):
# I.Unit test
# 	A. Views.py tets
	# def test_function_di_view_dipanggil_atau_tidak(self):
	# 	response = resolve(reverse('formStatusView'))
	# 	self.assertEqual(response.func, formStatusView)

	def test_url_ada_atau_nggak(self):
		response = self.client.get('')
		self.assertEqual(response.status_code, 200)

	def test_url_tidak_ada(self):
		response = Client().get('/invalid')
		self.assertEqual(response.status_code,404)

	def test_pakai_statuspage_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'statusPage.html')

	def test_request_POST_berhasil_disimpan(self):
		response = self.client.post('', data={'isi':'String Status'})
		new_response = self.client.get('/')
		html_response = new_response.content.decode('utf-8')
		self.assertIn('String Status', html_response)

	#B. Front End Tests
	def test_ada_title_halo_world(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn("Hello, How's your day?", content)

	def test_ada_label_untuk_menandai_form_yang_akan_diisi(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('Status: ', content)

	def test_ada_button_dengan_tulisan_share(self):
		response = Client().get('')
		content = response.content.decode('UTF8')
		self.assertIn('Share!', content)

	#C. Backend test
	#C.1. models.py tests
	def test_apakah_terdapat_model_WriteStatus(self):
		s = WriteStatus.objects.create(isi = "test string")
		self.assertEqual(str(s), "test string")

	def test_WriteStatus_buat_status_baru(self):
		new_stats= WriteStatus.objects.create(isi='Test membuat status baru')
		count_stats_obj = WriteStatus.objects.all().count()
		self.assertEqual(count_stats_obj, 1)

	def test_Write_Status_berhasil_di_simpan(self):
	 	response = Client().post('', data={'isi' : 'Test membuat status baru'})
	 	count_stats_obj = WriteStatus.objects.all().count()
	 	self.assertEqual(count_stats_obj, 1)
	 	self.assertEqual(response.status_code, 302)
	 	new_response = self.client.get('/')
	 	html_response = new_response.content.decode('utf-8')
	 	self.assertIn('Test membuat status baru', html_response)

	#C.2 forms.py tests
	def test_kalau_isinya_blank(self):
		form = FormStatus(data={'isi' : ' '})
		self.assertFalse(form.is_valid())

	def test_kalau_isinya_memenuhi_syarat(self):
		form = FormStatus(data={'isi' : 'Test jumlah kata kelebihan atau nggak. Ini contoh bener'})
		self.assertTrue(form.is_valid())

	def test_kalau_isinya_tidak_memenuhi_syarat(self):
		form = FormStatus(data={'isi' : 'lalala '*1000})
		self.assertFalse(form.is_valid())

# II. Functional Tests

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chromedriver = "./chromedriver"
		chrome_options = Options()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument("--disable-dev-shm-usage")
		self.selenium = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
	def tearDown(self):
		self.selenium.quit()
		super().tearDown()

	def test_apakah_title_sesuai(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		self.assertIn("Status",selenium.title)
