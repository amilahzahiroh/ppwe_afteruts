from django import forms
from statusPage import models

class FormStatus(forms.Form):
  isi = forms.CharField(max_length=300,
    label = "Status",
    widget = forms.TextInput())