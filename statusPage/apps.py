from django.apps import AppConfig


class StatuspageConfig(AppConfig):
    name = 'statusPage'
