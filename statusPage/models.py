from django.db import models
# Create your models here.

class WriteStatus(models.Model):
	"""docstring for ClassName"""
	isi = models.CharField(max_length=300)
	dateAndTime = models.DateTimeField(auto_now_add = True, blank = True)

	def __str__(self):
		return self.isi