from django.shortcuts import render, redirect
from statusPage import models, forms
import datetime

# Create your views here.
def formStatusView(request):
	if request.method == "POST":
		form = forms.FormStatus(request.POST)
		if form.is_valid():
			baru = models.WriteStatus(
				isi = form.data['isi']
				)
			baru.save()
			return redirect('/')
	else:
		form = forms.FormStatus()

	content = {'formkosong' : form, 'isi' : models.WriteStatus.objects.all().order_by('dateAndTime').reverse().values()}
	return render(request, 'statusPage.html', content)
